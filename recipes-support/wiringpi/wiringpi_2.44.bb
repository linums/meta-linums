DESCRIPTION = "A library to control Raspberry Pi GPIO channels"
HOMEPAGE = "https://projects.drogon.net/raspberry-pi/wiringpi/"
SECTION = "devel/libs"
LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://COPYING.LESSER;md5=e6a600fd5e1d9cbde2d983680233ad02"

DEPENDS += "virtual/crypt"

SRC_URI = "\
   file://wiringpi-2.44.tar.gz \
   file://0001-Add-initial-cross-compile-support.patch \
   file://0002-fix-combat-multiple-definition.patch \
"
SRC_URI[md5sum] = "1dcd8fb79d384b1da19d79268f29f362"

COMPATIBLE_MACHINE = "^rpi$"

CFLAGS_prepend = "-I${S}/wiringPi -I${S}/devLib "

EXTRA_OEMAKE += "'INCLUDE_DIR=${D}${includedir}' 'LIB_DIR=${D}${libdir}'"
EXTRA_OEMAKE += "'DESTDIR=${D}/usr' 'PREFIX=""'"

do_compile() {
    oe_runmake -C devLib
    oe_runmake -C wiringPi
    oe_runmake -C gpio 'LDFLAGS=${LDFLAGS} -L${S}/wiringPi -L${S}/devLib'
}

do_install() {
    oe_runmake -C devLib install
    oe_runmake -C wiringPi install
    oe_runmake -C gpio install
}

do_package_append() {
    pkgdest = d.getVar('PKGDEST', d, 1)
    pn = d.getVar('PN', d, 1)
    os.system('ln -sr ' + pkgdest + '/' + pn + '/usr/lib/libwiringPi.so. ' + pkgdest + '/' + pn + '/usr/lib/libwiringPi.so')
    os.system('ln -sr ' + pkgdest + '/' + pn + '/usr/lib/libwiringPiDev.so. ' + pkgdest + '/' + pn + '/usr/lib/libwiringPiDev.so')
}
